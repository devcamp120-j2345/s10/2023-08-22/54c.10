package models;

public class Triangle extends Shape {
    private int base;
    private int height;
    
    public Triangle(int base, int height) {
        this.base = base;
        this.height = height;
    }

    public Triangle(int base, int height, String color) {
        this.base = base;
        this.height = height;

        this.setColor(color);
    }

    public double getArea() {
        return this.base * this.height;
    } 

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String toString() {
        return "Triangle[base="+this.base+",height="+this.height+",color="+this.getColor()+"]";
    }
}
